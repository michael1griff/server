import boto3 
import json
        
def lambda_handler(event, _):
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps({"Message": "Create: Success"})
    }